package controllers

import (
	"go-crud/initializers"
	"go-crud/models"

	"github.com/gin-gonic/gin"
)

func PostsCreate (c *gin.Context) {
	// get data off request body
	var body struct {
		Title string
		Body string
	}

	c.Bind(&body)

	// create a post
	data := models.Post{Title: body.Title, Body: body.Title}

	query := initializers.DB.Create(&data)

	if query.Error != nil {
		c.Status(400)
		return
	}
	
	// return it
	c.JSON(200, gin.H{
		"post": data,
	})
}

func PostIndex(c *gin.Context) {
	// get the posts
	var posts []models.Post
	initializers.DB.Find(&posts)

	// respond with theme
	c.JSON(200, gin.H{
		"posts": posts,
	})
}

